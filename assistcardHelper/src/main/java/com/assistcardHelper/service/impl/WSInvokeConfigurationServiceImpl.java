package com.assistcardHelper.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assistcardHelper.dao.WSInvokeConfigurationDAO;
import com.assistcardHelper.model.WSInvokeConfiguration;
import com.assistcardHelper.util.NullValue;

@Service
public class WSInvokeConfigurationServiceImpl {

	private Map<String, String> wsInvMap;
	
	@Autowired
	WSInvokeConfigurationDAO wsInvDao;
	
	public void load() {
		List<WSInvokeConfiguration> wsInvokeLst = wsInvDao.findAll();
		if(!NullValue.isNullorIsZeroLength(wsInvokeLst)) {
			wsInvMap = new HashMap<String, String>();
			for (WSInvokeConfiguration item : wsInvokeLst) {
				wsInvMap.put(item.getKey(), item.getValue());
			}
		}
	}
	
	public Map<String, String> getInstance() {
		if(wsInvMap==null) {
			load();
		}
		return wsInvMap;
	}
	
	/**
	 * getStringValue
	 * @param key
	 * @return String
	 */
	public String getStringValue(String key) {
		if (wsInvMap.containsKey(key)) {
			return wsInvMap.get(key);
		}
		return null;
	}
	
}
