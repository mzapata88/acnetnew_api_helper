package com.assistcardHelper.util;

import java.util.Collection;
import java.util.List;

public class NullValue {
	
	public static boolean isNull (Object o) {
		return (o == null);
	}
	
	public static boolean isNotNull (Object o) {
		return (o != null);
	}
	
	public static boolean isZeroLength (String o) {
		return (o.length() == 0);
	}
	
	public static boolean isNullOrZeroLength (String o) {
		return (o == null || o.length() == 0);
	}
	
	public static boolean isNotNullOrZeroLength(String o) {
		return !isNullOrZeroLength(o);
	}
	
	public static boolean isNotZeroLength (String o) {
		return (o.length() > 0);
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isNotZeroLength (List o) {
		return (o.size() > 0);
	}
	
	@SuppressWarnings("rawtypes")
	public static boolean isNullorIsZeroLength (Collection o) {
		return isNull(o) || isZeroLength(o);
	}

	@SuppressWarnings("rawtypes")
	private static boolean isZeroLength(Collection o) {
		return (o.size() == 0);
	}

}