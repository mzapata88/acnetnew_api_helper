package com.assistcardHelper.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {
	
	public static final String timestampWithDash = "yyyy-MM-dd hh:mm:ss";
	public static final String mmSsSss = "mm:ss:SSS";
	
	/**
	 * getFormatDate
	 * @param <T>
	 * @param mask
	 * @return String
	 * @throws ParseException
	 */
	 public static <T> String getFormatDate(String mask, T pDate){
	    SimpleDateFormat df = new SimpleDateFormat(mask);
	    Calendar cal = Calendar.getInstance();
	    if(pDate!=null) {
	    	if(pDate!=null) {
		    	if(pDate.getClass().getTypeName().equals(Long.class.getTypeName())) {
		    		cal.setTimeInMillis(((Number) pDate).longValue());
		    	}
		    }
	    }
	    Date date = cal.getTime();
	    return df.format(date);
	 }
	 
}
