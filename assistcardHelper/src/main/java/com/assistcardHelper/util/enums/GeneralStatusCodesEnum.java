package com.assistcardHelper.util.enums;

public enum GeneralStatusCodesEnum {
	
	FilterIsNull(1001), //DTOFilter vino nulo

    //Combinaciones de parámetros : 1110-1129
    FilterCombinationAtLeastOneMissing(1110), //Se pide que uno de N parámetros venga con valor y no sucedió
    FilterCombinationOneAsTooManyExceded(1111), //Se pide que como mucho uno de N parámetros venga con valor y no sucedió
    FilterCombinationAllOrNoneMustBeAssigned(1112), //De la lista de N parámetros, deben venir todos o ninguno

    //Restricciones de parámetros : 1130-1149
    ParameterForbidden(1130), //Vino un parámetro prohibido
    ParameterForbiddenValue(1131), //Vino un parámetro con valor prohibido
    ParameterForcedValueNotMatch(1132), //Vino un parámetro con valor diferente al obligatorio
    
    //Errores de parámetros : 1150-1169
    ParameterMissing(1150), //Parámetro faltante
    ParameterUnableToDeserialize(1151), //No se puede deserializar un parámetro al tipo de valor esperado
    ParameterMustBePositive(1152), //Un parámetro que debería venir positivo, no sucedió.
    ParameterRequiredField(1153), //Campo requerido
    ParameterFieldLengthExceeded(1154),//Longitud del campo excedida

    //Errores de programación utilizando el helper de parámetros : 1170-1179
    FlagParameterMissingForParameterRestriction(1170), //Error de programación: No se pasó el parámetro requerido para una restricción, al llamar al helper de parámetros

    //Errores de datos obtenidos  : 1180-1199
    EntitySearchValueNotFound(1180), //Al traer una entidad por el valor de búsqueda por parámetro, no fue encontrada
    UnknownInheritedType(1181);  //El parámetro es de un Type no contemplado. Ej: se espera un tipo que hereda de Usuario, y se conocen dos clases que heredan de usuario, pero se recibió una tercera 
    
	private int code;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}

	GeneralStatusCodesEnum(int code) {
		this.code = code;
	}
}
