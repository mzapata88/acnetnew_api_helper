package com.assistcardHelper.util.exception;

import java.util.ArrayList;
import java.util.List;

import com.assistcardHelper.util.generic.response.dto.DTOApplicationErrorData;

public class CustomizedException extends RuntimeException {
	private static final long serialVersionUID = -2150082825370336803L;
	
	private DTOApplicationErrorData applicationErrorData;
	
	public CustomizedException(int statusCode, String applicationStatusCodeSource, String applicationEventTableName, 
			int idApplicationEventLog, java.util.List<String> statusCodeParameters){
		this(statusCode, applicationStatusCodeSource, applicationEventTableName, idApplicationEventLog, statusCodeParameters, null);
	}

	public CustomizedException(int statusCode, String applicationStatusCodeSource, String applicationEventTableName, 
			int idApplicationEventLog){
		this(statusCode, applicationStatusCodeSource, applicationEventTableName, idApplicationEventLog, null, null);
	}

	public CustomizedException(int statusCode, String applicationStatusCodeSource, String applicationEventTableName, 
			int idApplicationEventLog, List<String> statusCodeParameters, String message){
		setApplicationErrorData(new DTOApplicationErrorData());
		getApplicationErrorData().setMessage(message);
		getApplicationErrorData().setApplicationStatusCodeSource(applicationStatusCodeSource);
		getApplicationErrorData().setStatusCode(statusCode);
		getApplicationErrorData().setStatusCodeParameters(statusCodeParameters);
		getApplicationErrorData().setApplicationEventTableName(applicationEventTableName);
		getApplicationErrorData().setIdApplicationEventLog(idApplicationEventLog);
	}
	
	public CustomizedException(int statusCode, String applicationStatusCodeSource, ArrayList<String> statusCodeParameters, String message){
		setApplicationErrorData(new DTOApplicationErrorData());
		getApplicationErrorData().setMessage(message);
		getApplicationErrorData().setApplicationStatusCodeSource(applicationStatusCodeSource);
		getApplicationErrorData().setStatusCode(statusCode);
		getApplicationErrorData().setStatusCodeParameters(statusCodeParameters);
	}

	public CustomizedException(int statusCode, String applicationStatusCodeSource, ArrayList<String> statusCodeParameters){
		this(statusCode, applicationStatusCodeSource, statusCodeParameters, null);
	}

	public CustomizedException(int statusCode, String applicationStatusCodeSource)
	{
		this(statusCode, applicationStatusCodeSource, null, null);
	}
	
	public CustomizedException(int statusCode, String message, String applicationStatusCodeSource)
	{
		this(statusCode, applicationStatusCodeSource, null, message);
	}

	public CustomizedException(DTOApplicationErrorData applicationErrorData){
		super(applicationErrorData.getMessage());
		setApplicationErrorData(applicationErrorData);
	}

	public DTOApplicationErrorData getApplicationErrorData() {
		return applicationErrorData;
	}

	public void setApplicationErrorData(DTOApplicationErrorData applicationErrorData) {
		this.applicationErrorData = applicationErrorData;
	}

	public final String getApplicationEventLogUICode(){
		//return ResponseTypesHelper.GetApplicationEventLogUICode(getApplicationErrorData());
		//TODO: IMPLEMENTAR RESPONSETYPESHELPER
		return "implementar ResponseTypesHelper";
	}

}
