package com.assistcardHelper.util.generic.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOResponseListScalar<T> extends DTOResponseSearchBase{

	@JsonProperty("ResponseList")
	private List<T> responseListScalar;
	
	public static <T> DTOResponseListScalar<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, null);
	}
	
	public static <T> DTOResponseListScalar<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(statusCode, null);
	}

	public static <T> DTOResponseListScalar<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode, String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getBadRequestErrorResponse(){
		return getBadRequestErrorResponse(null);
	}

	public static <T> DTOResponseListScalar<T> getBadRequestErrorResponse(String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(BAD_REQUEST_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseListScalar<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getCustomizedErrorCodeResponse(int customStatusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(customStatusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseListScalar<T> getCustomizedErrorCodeResponse(int customStatusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(customStatusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getErrorResponse(String applicationEventTableName, int errorId){
		return getErrorResponse(applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseListScalar<T> getErrorResponse(String applicationEventTableName, int errorId, String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(ERROR_STATUS);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getNotFoundErrorResponse(){
		return getNotFoundErrorResponse(null);
	}

	public static <T> DTOResponseListScalar<T> getNotFoundErrorResponse(String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(NOT_FOUND_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getForbiddenAccessResponse(){
		return getForbiddenAccessResponse(null);
	}

	public static <T> DTOResponseListScalar<T> getForbiddenAccessResponse(String message){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(FORBIDDEN_ACCESS_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getApplicationErrorDataResponse(DTOApplicationErrorData applicationErrorData){
		DTOResponseListScalar<T> tempVar = new DTOResponseListScalar<T>();
		tempVar.setStatusCode(applicationErrorData.getStatusCode());
		tempVar.setApplicationStatusCodeSource(applicationErrorData.getApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationErrorData.getApplicationEventTableName());
		tempVar.setErrorId(applicationErrorData.getIdApplicationEventLog());
		tempVar.setMessage(applicationErrorData.getMessage());
		tempVar.setStatusCodeParameters(applicationErrorData.getStatusCodeParameters());
		return tempVar;
	}

	public static <T> DTOResponseListScalar<T> getListScalarOkResponse(List<T> underlyingScalarList){
		DTOResponseListScalar<T> dtoResponseScalarList = new DTOResponseListScalar<T>();
		dtoResponseScalarList.setStatusCode(EXECUTED_OK_STATUS);
		dtoResponseScalarList.setResponseListScalar(underlyingScalarList);
		return dtoResponseScalarList;
	}

	public List<T> getResponseListScalar() {
		return responseListScalar;
	}
	public void setResponseListScalar(List<T> responseListScalar) {
		this.responseListScalar = responseListScalar;
	}

}
