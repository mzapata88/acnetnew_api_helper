package com.assistcardHelper.util.generic.response.dto;

import java.util.List;

public class DTOResponseExecution extends DTOResponseBase{
	
	public static DTOResponseExecution getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, null);
	}

	public static DTOResponseExecution getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String message){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseExecution getNotPersistedCustomizedErrorCodeResponse(int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(statusCode, null);
	}

	public static DTOResponseExecution getNotPersistedCustomizedErrorCodeResponse(int statusCode, String message){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setMessage(message);
		return tempVar;
	}
	
	public static DTOResponseExecution getNotPersistedCustomizedErrorCodeResponse(int statusCode, String message, List<String> statusCodeParameters){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setMessage(message);
		tempVar.setStatusCodeParameters(statusCodeParameters);
		return tempVar;
	}

	public static DTOResponseExecution getBadRequestErrorResponse(){
		return getBadRequestErrorResponse(null);
	}

	public static DTOResponseExecution getBadRequestErrorResponse(String message){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(BAD_REQUEST_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseExecution getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, applicationEventTableName, errorId, null);
	}

	public static DTOResponseExecution getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseExecution getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(statusCode, applicationEventTableName, errorId, null);
	}

	public static DTOResponseExecution getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseExecution getErrorResponse(String applicationEventTableName, int errorId){
		return getErrorResponse(applicationEventTableName, errorId, null);
	}

	public static DTOResponseExecution getErrorResponse(String applicationEventTableName, int errorId, String message){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(ERROR_STATUS);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseExecution getForbiddenAccessResponse(){
		return getForbiddenAccessResponse(null);
	}

	public static DTOResponseExecution getForbiddenAccessResponse(String message){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(FORBIDDEN_ACCESS_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseExecution getApplicationErrorDataResponse(DTOApplicationErrorData applicationErrorData){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(applicationErrorData.getStatusCode());
		tempVar.setApplicationStatusCodeSource(applicationErrorData.getApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationErrorData.getApplicationEventTableName());
		tempVar.setErrorId(applicationErrorData.getIdApplicationEventLog());
		tempVar.setMessage(applicationErrorData.getMessage());
		tempVar.setStatusCodeParameters(applicationErrorData.getStatusCodeParameters());
		return tempVar;
	}

	public static DTOResponseExecution getExecutedOkResponse(){
		DTOResponseExecution tempVar = new DTOResponseExecution();
		tempVar.setStatusCode(EXECUTED_OK_STATUS);
		return tempVar;
	}
}
