package com.assistcardHelper.util.generic.response.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOResponseInsert extends DTOResponseBase{

	public static int INSERTED_OK = 201;
	
	@JsonProperty("RegisteredId")
	private int registeredId;
	
	public int getRegisteredId() {
		return registeredId;
	}
	public void setRegisteredId(int registeredId) {
		this.registeredId = registeredId;
	}

	public static DTOResponseInsert getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, null);
	}

	public static DTOResponseInsert getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String message){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseInsert getNotPersistedCustomizedErrorCodeResponse(int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(statusCode, null);
	}

	public static DTOResponseInsert getNotPersistedCustomizedErrorCodeResponse(int statusCode, String message){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setMessage(message);
		return tempVar;
	}
	
	public static DTOResponseInsert getBadRequestErrorResponse(){
		return getBadRequestErrorResponse(null);
	}

	public static DTOResponseInsert getBadRequestErrorResponse(String message){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(BAD_REQUEST_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseInsert getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, applicationEventTableName, errorId, null);
	}

	public static DTOResponseInsert getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseInsert getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(statusCode, applicationEventTableName, errorId, null);
	}

	public static DTOResponseInsert getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseInsert getErrorResponse(String applicationEventTableName, int errorId){
		return getErrorResponse(applicationEventTableName, errorId, null);
	}

	public static DTOResponseInsert getErrorResponse(String applicationEventTableName, int errorId, String message){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(ERROR_STATUS);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseInsert getForbiddenAccessResponse(){
		return getForbiddenAccessResponse(null);
	}

	public static DTOResponseInsert getForbiddenAccessResponse(String message){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(FORBIDDEN_ACCESS_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static DTOResponseInsert getApplicationErrorDataResponse(DTOApplicationErrorData applicationErrorData){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setStatusCode(applicationErrorData.getStatusCode());
		tempVar.setApplicationStatusCodeSource(applicationErrorData.getApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationErrorData.getApplicationEventTableName());
		tempVar.setErrorId(applicationErrorData.getIdApplicationEventLog());
		tempVar.setMessage(applicationErrorData.getMessage());
		tempVar.setStatusCodeParameters(applicationErrorData.getStatusCodeParameters());
		return tempVar;
	}

	public static DTOResponseInsert getInsertedOkResponse(int registeredId){
		DTOResponseInsert tempVar = new DTOResponseInsert();
		tempVar.setRegisteredId(registeredId);
		tempVar.setStatusCode(INSERTED_OK);
		return tempVar;
	}
}
