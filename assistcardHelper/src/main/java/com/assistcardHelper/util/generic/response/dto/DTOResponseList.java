package com.assistcardHelper.util.generic.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOResponseList<T> extends DTOResponseSearchBase{
	
	@JsonProperty("ResponseList")
	private List<T> responseList;
	
	public static <T> DTOResponseList<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, null);
	}

	public static <T> DTOResponseList<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(statusCode, null);
	}

	public static <T> DTOResponseList<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode, String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getBadRequestErrorResponse(){
		return getBadRequestErrorResponse(null);
	}

	public static <T> DTOResponseList<T> getBadRequestErrorResponse(String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(BAD_REQUEST_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseList<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(statusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseList<T> getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getErrorResponse(String applicationEventTableName, int errorId){
		return getErrorResponse(applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseList<T> getErrorResponse(String applicationEventTableName, int errorId, String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(ERROR_STATUS);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getNotFoundErrorResponse(){
		return getNotFoundErrorResponse(null);
	}

	public static <T> DTOResponseList<T> getNotFoundErrorResponse(String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(NOT_FOUND_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getForbiddenAccessResponse(){
		return getForbiddenAccessResponse(null);
	}

	public static <T> DTOResponseList<T> getForbiddenAccessResponse(String message){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(FORBIDDEN_ACCESS_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseList<T> getApplicationErrorDataResponse(DTOApplicationErrorData applicationErrorData){
		DTOResponseList<T> tempVar = new DTOResponseList<T>();
		tempVar.setStatusCode(applicationErrorData.getStatusCode());
		tempVar.setApplicationStatusCodeSource(applicationErrorData.getApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationErrorData.getApplicationEventTableName());
		tempVar.setErrorId(applicationErrorData.getIdApplicationEventLog());
		tempVar.setMessage(applicationErrorData.getMessage());
		tempVar.setStatusCodeParameters(applicationErrorData.getStatusCodeParameters());
		return tempVar;
	}

	public static <T> DTOResponseList<T> getListOkResponse(List<T> underlyingList){
		DTOResponseList<T> dtoResponseList = new DTOResponseList<T>();
		dtoResponseList.setStatusCode(EXECUTED_OK_STATUS);
		dtoResponseList.setResponseList(underlyingList);
		return dtoResponseList;
	}

	public List<T> getResponseList() {
		return responseList;
	}
	public void setResponseList(List<T> responseList) {
		this.responseList = responseList;
	}
}
