package com.assistcardHelper.util.generic.response.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOResponseScalar<T> extends DTOResponseSearchBase{
	
	@JsonProperty("ResponseScalar")
	private T responseScalar;
	
	public static <T> DTOResponseScalar<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, null);
	}

	public static <T> DTOResponseScalar<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(statusCode, null);
	}

	public static <T> DTOResponseScalar<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode, String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getBadRequestErrorResponse(){
		return getBadRequestErrorResponse(null);
	}

	public static <T> DTOResponseScalar<T> getBadRequestErrorResponse(String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(BAD_REQUEST_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseScalar<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(statusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseScalar<T> getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getErrorResponse(String applicationEventTableName, int errorId){
		return getErrorResponse(applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseScalar<T> getErrorResponse(String applicationEventTableName, int errorId, String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(ERROR_STATUS);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getNotFoundErrorResponse(){
		return getNotFoundErrorResponse(null);
	}

	public static <T> DTOResponseScalar<T> getNotFoundErrorResponse(String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(NOT_FOUND_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getForbiddenAccessResponse(){
		return getForbiddenAccessResponse(null);
	}

	public static <T> DTOResponseScalar<T> getForbiddenAccessResponse(String message){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(FORBIDDEN_ACCESS_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getApplicationErrorDataResponse(DTOApplicationErrorData applicationErrorData){
		DTOResponseScalar<T> tempVar = new DTOResponseScalar<T>();
		tempVar.setStatusCode(applicationErrorData.getStatusCode());
		tempVar.setApplicationStatusCodeSource(applicationErrorData.getApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationErrorData.getApplicationEventTableName());
		tempVar.setErrorId(applicationErrorData.getIdApplicationEventLog());
		tempVar.setStatusCodeParameters(applicationErrorData.getStatusCodeParameters());
		tempVar.setMessage(applicationErrorData.getMessage());
		return tempVar;
	}

	public static <T> DTOResponseScalar<T> getScalarOkResponse(T underlyingScalar){
		DTOResponseScalar<T> dtoResponseScalar = new DTOResponseScalar<T>();
		dtoResponseScalar.setStatusCode(EXECUTED_OK_STATUS);
		dtoResponseScalar.setResponseScalar(underlyingScalar);
		return dtoResponseScalar;
	}

	public T getResponseScalar() {
		return responseScalar;
	}
	public void setResponseScalar(T responseScalar) {
		this.responseScalar = responseScalar;
	}
}
