package com.assistcardHelper.util.generic.response.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOResponseEntity<T> extends DTOResponseSearchBase{
	
	@JsonProperty("ResponseEntity")
	private T responseEntity;
	
	private DTOResponseEntity(){
	}

	public static <T> DTOResponseEntity<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, null);
	}

	public static <T> DTOResponseEntity<T> getNotPersistedCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setMessage(message);
		return tempVar;
	}
	
	public static <T> DTOResponseEntity<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode){
		return getNotPersistedCustomizedErrorCodeResponse(statusCode, null);
	}

	public static <T> DTOResponseEntity<T> getNotPersistedCustomizedErrorCodeResponse(int statusCode, String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getBadRequestErrorResponse(){
		return getBadRequestErrorResponse(null);
	}

	public static <T> DTOResponseEntity<T> getBadRequestErrorResponse(String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(BAD_REQUEST_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(applicationStatusCodeSource, statusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseEntity<T> getCustomizedErrorCodeResponse(String applicationStatusCodeSource, int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(applicationStatusCodeSource);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId){
		return getCustomizedErrorCodeResponse(statusCode, applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseEntity<T> getCustomizedErrorCodeResponse(int statusCode, String applicationEventTableName, int errorId, String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(statusCode);
		tempVar.setApplicationStatusCodeSource(getCurrentApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getErrorResponse(String applicationEventTableName, int errorId){
		return getErrorResponse(applicationEventTableName, errorId, null);
	}

	public static <T> DTOResponseEntity<T> getErrorResponse(String applicationEventTableName, int errorId, String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(ERROR_STATUS);
		tempVar.setApplicationEventTableName(applicationEventTableName);
		tempVar.setErrorId(errorId);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getNotFoundErrorResponse(){
		return getNotFoundErrorResponse(null);
	}

	public static <T> DTOResponseEntity<T> getNotFoundErrorResponse(String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(NOT_FOUND_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getForbiddenAccessResponse(){
		return getForbiddenAccessResponse(null);
	}

	public static <T> DTOResponseEntity<T> getForbiddenAccessResponse(String message){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(FORBIDDEN_ACCESS_STATUS);
		tempVar.setMessage(message);
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getApplicationErrorDataResponse(DTOApplicationErrorData applicationErrorData){
		DTOResponseEntity<T> tempVar = new DTOResponseEntity<T>();
		tempVar.setStatusCode(applicationErrorData.getStatusCode());
		tempVar.setApplicationStatusCodeSource(applicationErrorData.getApplicationStatusCodeSource());
		tempVar.setApplicationEventTableName(applicationErrorData.getApplicationEventTableName());
		tempVar.setErrorId(applicationErrorData.getIdApplicationEventLog());
		tempVar.setMessage(applicationErrorData.getMessage());
		tempVar.setStatusCodeParameters(applicationErrorData.getStatusCodeParameters());
		return tempVar;
	}

	public static <T> DTOResponseEntity<T> getEntityOkResponse(T underlyingEntity){
		DTOResponseEntity<T> dtoResponseEntity = new DTOResponseEntity<T>();
		dtoResponseEntity.setStatusCode(EXECUTED_OK_STATUS);
		dtoResponseEntity.setResponseEntity(underlyingEntity);
		return dtoResponseEntity;
	}

	public T getResponseEntity() {
		return responseEntity;
	}
	public void setResponseEntity(T responseEntity) {
		this.responseEntity = responseEntity;
	}
}
