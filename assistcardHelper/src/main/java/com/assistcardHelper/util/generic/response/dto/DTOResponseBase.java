package com.assistcardHelper.util.generic.response.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOResponseBase {
	public static String GLOBALCODE = "Global";
	public static int BAD_REQUEST_STATUS = 400;
	public static int ERROR_STATUS = 500;
	public static int FORBIDDEN_ACCESS_STATUS = 403;
	public static int EXECUTED_OK_STATUS = 200;
	public static String GENERALSOURCECODE = "GLB";

	@JsonProperty("ErrorId")
	private int errorId;
	
	@JsonProperty("StatusCode")
	private int statusCode;

	@JsonProperty("ApplicationEventTableName")
	private String applicationEventTableName;
	
	@JsonProperty("ApplicationStatusCodeSource")
	private String applicationStatusCodeSource;
	
	@JsonProperty("Message")
	private String message;
	
	@JsonProperty("StatusCodeParameters")
	private List<String> statusCodeParameters;
	
	protected DTOResponseBase(){
	}
	public int getErrorId() {
		return errorId;
	}
	public void setErrorId(int errorId) {
		this.errorId = errorId;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getApplicationEventTableName() {
		return applicationEventTableName;
	}
	public void setApplicationEventTableName(String applicationEventTableName) {
		this.applicationEventTableName = applicationEventTableName;
	}
	public String getApplicationStatusCodeSource() {
		return applicationStatusCodeSource;
	}
	public void setApplicationStatusCodeSource(String applicationStatusCodeSource) {
		this.applicationStatusCodeSource = applicationStatusCodeSource;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<String> getStatusCodeParameters() {
		return statusCodeParameters;
	}
	public void setStatusCodeParameters(List<String> statusCodeParameters) {
		this.statusCodeParameters = statusCodeParameters;
	}
	public static String getCurrentApplicationStatusCodeSource(){
	/*	Object applicationName = AppSettings.getConfiguration().getString("ApplicationName");
		if (applicationName != null){
			return applicationName.toString();
		}*/
		return "";
	}
}
