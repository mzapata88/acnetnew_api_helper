package com.assistcardHelper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssistcardHelperApplication {

	public static void main(String[] args) {
		SpringApplication.run(AssistcardHelperApplication.class, args);
	}
	
}
