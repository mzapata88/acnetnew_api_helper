package com.assistcardHelper.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.assistcardHelper.dtoMapper.ApplicationEventLogHelper;
import com.assistcardHelper.dtoMapper.DTOFilter;
import com.assistcardHelper.util.exception.CustomizedException;
import com.assistcardHelper.util.generic.response.dto.DTOApplicationErrorData;
import com.assistcardHelper.util.generic.response.dto.DTOResponseScalar;

@RestController
public class MainController {

	
	@RequestMapping(value="/getException",method=RequestMethod.GET)
	public DTOResponseScalar<Integer> getException(DTOFilter filter){
		try {
			Integer result = 1/0;
			return DTOResponseScalar.getScalarOkResponse(result);
		}catch (CustomizedException ex){
            DTOApplicationErrorData dtoApplicationErrorData = ex.getApplicationErrorData();
            ApplicationEventLogHelper.logApplicationErrorDataCatched(dtoApplicationErrorData);
            return DTOResponseScalar.getApplicationErrorDataResponse(dtoApplicationErrorData);
        }catch (Exception ex){
    	  DTOApplicationErrorData applicationErrorData = ApplicationEventLogHelper.logErrorCatched(ex, filter);
          return DTOResponseScalar.getApplicationErrorDataResponse(applicationErrorData);
        }
		
	}
	
}
