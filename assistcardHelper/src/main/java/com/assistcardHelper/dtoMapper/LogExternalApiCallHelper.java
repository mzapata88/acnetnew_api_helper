package com.assistcardHelper.dtoMapper;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import com.assistcardHelper.util.NullValue;
import com.assistcardHelper.util.generic.response.dto.DTOApplicationErrorData;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class LogExternalApiCallHelper {
	private static final int PARAMETERMAXLENGTH = 100; //si el valor de un parámetro que queda en AdditionalParams es más largo que esto, lo corto. Para no explotar demasiado la db
	private static final int REQUEST_ABSOLUTE_URL_DB_LENGTH = 50; //largo en las tablas de sql.
	private static final int REQUEST_RELATIVE_URL_DB_LENGTH = 50; //largo en las tablas de sql.
	private static final int COREPARAMETERSDBLENGTH = 50; //largo en las tablas de sql.


	public static void LogExternalApiCall(int logApiCallsStatus, String requestAbsoluteUrl, String requestRelativeUrl, String coreParameters, String jsonRequest, String jsonResponse, Timestamp dateTimeBeforeApiCall, Timestamp dateTimeAfterApiCall, String visitorIp, java.util.Map<String, Object> additionalParams){
		LogExternalApiCall(logApiCallsStatus, requestAbsoluteUrl, requestRelativeUrl, coreParameters, jsonRequest, jsonResponse, dateTimeBeforeApiCall, dateTimeAfterApiCall, visitorIp, additionalParams, 0);
	}

	public static void LogExternalApiCall(int logApiCallsStatus, String requestAbsoluteUrl, String requestRelativeUrl, String coreParameters, String jsonRequest, String jsonResponse, Timestamp dateTimeBeforeApiCall, Timestamp dateTimeAfterApiCall, String visitorIp){
		LogExternalApiCall(logApiCallsStatus, requestAbsoluteUrl, requestRelativeUrl, coreParameters, jsonRequest, jsonResponse, dateTimeBeforeApiCall, dateTimeAfterApiCall, visitorIp, null, 0);
	}

	public static void LogExternalApiCall(int logApiCallsStatus, String requestAbsoluteUrl, String requestRelativeUrl, String coreParameters, String jsonRequest, String jsonResponse, Timestamp dateTimeBeforeApiCall, Timestamp dateTimeAfterApiCall, String visitorIp, Map<String, Object> additionalParams, int paramsLength){
		long responseTimeMs = (dateTimeAfterApiCall.getTime() - dateTimeBeforeApiCall.getTime());
		if (ApplicationEventLogConfigurationHelper.getApplicationEventLogApi()!=null && ApplicationEventLogConfigurationHelper.getApplicationEventsTablesPrefix()!=null){
			(new Thread(() ->
			{
				try{
					Thread.currentThread().setDaemon(true);//equivalent to IsBackground 
					Integer idJsonRequest = null;
					Integer idJsonResponse = null;
					if (logApiCallsStatus == 2){
						String tableCommunicationsLog = ApplicationEventLogConfigurationHelper.getApplicationEventsTablesPrefix() + "CommunicationsLog";
						idJsonRequest = LogExternalApiCall(jsonRequest, tableCommunicationsLog).getIdApplicationEventLog();
						idJsonResponse = LogExternalApiCall(jsonResponse, tableCommunicationsLog).getIdApplicationEventLog();
					}
					DTOFilter filter = new DTOFilter();
					String applicationCallTableName = ApplicationEventLogConfigurationHelper.getApplicationEventsTablesPrefix() + "ApiCall";
					//normalizacion
					if (applicationCallTableName.contains("ApiApiCall")){
						applicationCallTableName = applicationCallTableName.replace("ApiApiCall", "ApiCall");
					}
					//
					filter.getParameters().put("ApplicationEventsTableName", applicationCallTableName);
					filter.getParameters().put("RequestAbsoluteUrl", TruncateField(requestAbsoluteUrl, REQUEST_ABSOLUTE_URL_DB_LENGTH));
					filter.getParameters().put("RequestRelativeUrl", TruncateField(requestRelativeUrl, REQUEST_RELATIVE_URL_DB_LENGTH));
					filter.getParameters().put("CoreParameters", TruncateField(coreParameters, COREPARAMETERSDBLENGTH));
					if (idJsonRequest != null){
						filter.getParameters().put("IdJsonRequest", idJsonRequest);
					}
					if (idJsonResponse != null){
						filter.getParameters().put("IdJsonResponse", idJsonResponse);
					}
					filter.getParameters().put("ResponseTimeMs", responseTimeMs);
					filter.setVisitorIp(visitorIp);
					if (ApplicationEventLogConfigurationHelper.getApplicationName() != null){
						filter.setApplicationName(ApplicationEventLogConfigurationHelper.getApplicationName());
					}
					int parametersLenght = paramsLength > 0 ? paramsLength : PARAMETERMAXLENGTH;
					if (additionalParams != null){
						for (Map.Entry<String, Object> parameter : additionalParams.entrySet()){
							//Este método va a recursivamente grabar toda la data, de parámetros flat, y Dictionary<string, object> que se encuentren dentro
							AddAdditionalParameter(parametersLenght, parameter, filter);
						}
					}	
					LogApplicationEvent(filter);
				}
				catch (RuntimeException ex){
					ApplicationEventLogHelper.logErrorCatched(ex);
				}
			})).start();
		}
	}

	private static DTOApplicationErrorData LogExternalApiCall(String requestData, String applicationEventsTableName){
		DTOFilter filter = new DTOFilter();
		filter.getParameters().put("ApplicationEventsTableName", applicationEventsTableName);
		filter.getParameters().put("Data", requestData);
		return LogApplicationEvent(filter);
	}
	
	private static DTOApplicationErrorData LogApplicationEvent(DTOFilter filter){
		String applicationEventLogApi = ApplicationEventLogConfigurationHelper.getApplicationEventLogApi();
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		String relativeUrl = "Logger/LogApplicationEvent";
		String jsonContext = gson.toJson(filter);
		String response = ApiHelper.post(applicationEventLogApi, relativeUrl, jsonContext);
		return ResponseTypesHelper.responseEntity(response, DTOApplicationErrorData.class);
	}

	@SuppressWarnings("unchecked")
	private static void AddAdditionalParameter(int parametersLenght, Map.Entry<String, Object> parameter, DTOFilter logErrorFilter){
		if (parameter.getValue() != null && HashMap.class.isAssignableFrom(parameter.getValue().getClass())){
			HashMap<String, Object> innerDictionaryParameters = (HashMap<String, Object>) parameter.getValue();
			if (innerDictionaryParameters != null){
				for (Map.Entry<String, Object> innerParameters : innerDictionaryParameters.entrySet()){
					AddAdditionalParameter(parametersLenght, innerParameters, logErrorFilter);
				}
			}
		}else{
			if (!logErrorFilter.getParameters().containsKey(parameter.getKey())){
				String parameterValue = null; //valor del parámetro que voy a considerar. Como máximo parameterLengthMax de largo
				if (parameter.getValue() != null && !NullValue.isNullOrZeroLength(parameter.getValue().toString())){
					if (parameter.getValue().toString().length() > parametersLenght){
						parameterValue = parameter.getValue().toString().substring(0, parametersLenght);
					}else{
						parameterValue = parameter.getValue().toString();
					}
				}
				if (!logErrorFilter.getParameters().containsKey(parameter.getKey())){
					logErrorFilter.getParameters().put(parameter.getKey(), parameterValue);
				}
			}
		}
	}

	@SuppressWarnings("unused")
	private static DTOApplicationErrorData LogExternalApiCall(String requestData, String tableCommunicationsLog, String applicationEventLogApi){
		DTOFilter filter = new DTOFilter();
		filter.getParameters().put("ApplicationEventsTableName", tableCommunicationsLog);
		filter.getParameters().put("Data", requestData);
		return ApplicationEventLogService.logEvent(ApplicationEventLogConfigurationHelper.getApplicationEventLogApi(), filter);
	}

	private static String TruncateField(String fieldOriginalValue, int fieldMaxLength){
		if ( NullValue.isNullOrZeroLength(fieldOriginalValue) || fieldOriginalValue.length() < fieldMaxLength){
			return fieldOriginalValue;
		}
		return fieldOriginalValue.substring(0, fieldMaxLength);
	}
}
