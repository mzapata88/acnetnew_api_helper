package com.assistcardHelper.dtoMapper;

import java.util.List;

public class DTOAppConfigurationFile extends DTOBase{
	
	private String fileName;
	public List<DTOAppSetting> appSettings;
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public List<DTOAppSetting> getAppSettings() {
		return appSettings;
	}
	public void setAppSettings(List<DTOAppSetting> appSettings) {
		this.appSettings = appSettings;
	}
	
}
