package com.assistcardHelper.dtoMapper;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOFilter extends DTOBase{
	
	@JsonProperty("Parameters")
	private Map<String, Object> parameters;
	
	@JsonProperty("VisitorIp")
	private String visitorIp;
	
	@JsonProperty("ApplicationName")
	private String applicationName;
	
	public DTOFilter() {
		parameters = new HashMap<String, Object>();
	}
	public Map<String, Object> getParameters() {
		return parameters;
	}
	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}
	public String getVisitorIp() {
		return visitorIp;
	}
	public void setVisitorIp(String visitorIp) {
		this.visitorIp = visitorIp;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
}
