package com.assistcardHelper.dtoMapper;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DTOBase {

	@JsonProperty("CurrentUICulture")
	public String currentUICulture;

	public String getCurrentUICulture() {
		return currentUICulture;
	}
	public void setCurrentUICulture(String currentUICulture) {
		this.currentUICulture = currentUICulture;
	}
//	public DTOBase(){
//		this.currentUICulture = LocaleContextHolder.getLocale().getDisplayLanguage();
//	}
}
