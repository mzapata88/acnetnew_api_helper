package com.assistcardHelper.dtoMapper;

import com.assistcardHelper.util.generic.response.dto.DTOApplicationErrorData;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ApplicationEventLogService {
	public static DTOApplicationErrorData logEvent(String applicationEventLogApi, DTOFilter filterLog){
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		String relativeUrl = "Logger/LogApplicationEvent";
		String jsonContext = gson.toJson(filterLog);
		String response = ApiHelper.post(applicationEventLogApi, relativeUrl, jsonContext);
		return ResponseTypesHelper.responseEntity(response, DTOApplicationErrorData.class);
	}
}
