package com.assistcardHelper.dtoMapper;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.assistcardHelper.configuration.WSInvokeConfigurationConst;
import com.assistcardHelper.service.impl.WSInvokeConfigurationServiceImpl;
import com.assistcardHelper.util.generic.response.dto.DTOApplicationErrorData;
import com.assistcardHelper.util.generic.response.dto.DTOResponseBase;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class ApplicationEventLogHelper {

	private static final int PARAMETERMAXLENGTH = 100;
	private static final int STACKTRACEMAXLENGTH = 1000;
	
	private static WSInvokeConfigurationServiceImpl wSInvokeConfServiceImpl;
	
	@Autowired
	public ApplicationEventLogHelper(WSInvokeConfigurationServiceImpl wsInvoke) {
		ApplicationEventLogHelper.wSInvokeConfServiceImpl = wsInvoke;
	}

	public static DTOApplicationErrorData logError(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams, int eventSeverity, String eventOrigin, String visitorIp){
		return logError(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, eventOrigin, visitorIp, 0);
	}

	public static DTOApplicationErrorData logError(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams, int eventSeverity, String eventOrigin){
		return logError(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, eventOrigin, null, 0);
	}

	public static DTOApplicationErrorData logError(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams, int eventSeverity){
		return logError(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, null, null, 0);
	}

	public static DTOApplicationErrorData logError(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams){
		return logError(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, 5, null, null, 0);
	}

	public static DTOApplicationErrorData logError(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage){
		return logError(exception, inputFilter, customizedEventTitle, customizedEventMessage, null, 5, null, null, 0);
	}

	public static DTOApplicationErrorData logError(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle){
		return logError(exception, inputFilter, customizedEventTitle, null, null, 5, null, null, 0);
	}

	public static DTOApplicationErrorData logError(RuntimeException exception, DTOFilter inputFilter){
		return logError(exception, inputFilter, null, null, null, 5, null, null, 0);
	}

	public static DTOApplicationErrorData logError(RuntimeException exception){
		return logError(exception, null, null, null, null, 5, null, null, 0);
	}

	public static DTOApplicationErrorData logError(){
		return logError(null, null, null, null, null, 5, null, null, 0);
	}
	
	/**
	 * logError
	 * @param exception
	 * @param inputFilter
	 * @param customizedEventTitle
	 * @param customizedEventMessage
	 * @param additionalParams
	 * @param eventSeverity
	 * @param eventOrigin
	 * @param visitorIp
	 * @param paramsLength
	 * @return DTOApplicationErrorData
	 */
	public static DTOApplicationErrorData logError(Exception exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, 
			Map<String, Object> additionalParams, int eventSeverity, String eventOrigin, String visitorIp, int paramsLength){
		
		ApplicationProps ap = new ApplicationProps();
		getEnviromentLogConfigurationKeys(ap);
		if (ap.getApplicationEventLogApi().isEmpty()){
			DTOApplicationErrorData tempVar = new DTOApplicationErrorData();
			tempVar.setStatusCode(DTOResponseBase.ERROR_STATUS);
			tempVar.setApplicationStatusCodeSource(DTOResponseBase.GLOBALCODE);
			tempVar.setMessage("No se encuentra configurada la api centralizadora de eventos");
			return tempVar;
		}
		DTOFilter logErrorFilter = new DTOFilter();
		logErrorFilter.setApplicationName(ap.getApplicationName());
		logErrorFilter.setVisitorIp(StringUtils.isNotEmpty(inputFilter.getVisitorIp())?inputFilter.getVisitorIp():"");
		logErrorFilter.getParameters().put("ApplicationEventsTableName", ap.getApplicationEventsTableName());
		logErrorFilter.getParameters().put("EventType", "Error");
		logErrorFilter.getParameters().put("EventSeverity", eventSeverity);
		if (StringUtils.isNotEmpty(eventOrigin)){
			logErrorFilter.getParameters().put("EventOrigin", eventOrigin);
		}
		if (exception != null){
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			exception.printStackTrace(pw);
			logErrorFilter.getParameters().put("ExceptionType", exception.getClass().toString());
			logErrorFilter.getParameters().put("ExceptionMessage", exception.getMessage());
			logErrorFilter.getParameters().put("Stack", sw.toString().substring(0, STACKTRACEMAXLENGTH));
		}
		if (StringUtils.isNotEmpty(customizedEventTitle)){
			logErrorFilter.getParameters().put("CustomizedEventTitle", customizedEventTitle);
		}
		if (StringUtils.isNotEmpty(customizedEventMessage)){
			logErrorFilter.getParameters().put("CustomizedEventMessage", customizedEventMessage);
		}

		int parametersLenght = paramsLength > 0 ? paramsLength : PARAMETERMAXLENGTH;

		if (additionalParams != null){
			for (Map.Entry<String, Object> parameter : additionalParams.entrySet()){
				if (!logErrorFilter.getParameters().containsKey(parameter.getKey())){
					String parameterValue = null; //valor del parámetro que voy a considerar. Como máximo parameterLengthMax de largo
					if (StringUtils.isNotEmpty(parameter.getValue().toString())){
						if (parameter.getValue().toString().length() > parametersLenght){
							parameterValue = parameter.getValue().toString().substring(0, parametersLenght);
						}else{
							parameterValue = parameter.getValue().toString();
						}
					}
					logErrorFilter.getParameters().put(parameter.getKey(), parameterValue);
				}
			}
		}

		if (inputFilter != null){
			if (StringUtils.isNotEmpty(inputFilter.getApplicationName())){
				logErrorFilter.getParameters().put("ExternalOriginApplication", inputFilter.getApplicationName());
			}
			if (StringUtils.isEmpty(logErrorFilter.getVisitorIp()) && !StringUtils.isNotEmpty(inputFilter.getVisitorIp())){
				logErrorFilter.getParameters().put("VisitorIp", inputFilter.getVisitorIp());
				logErrorFilter.setVisitorIp(inputFilter.getVisitorIp());
			}
			for (Map.Entry<String, Object> parameter : inputFilter.getParameters().entrySet()){
				if (!logErrorFilter.getParameters().containsKey(parameter.getKey())){
					String parameterValue = null; //valor del parámetro que voy a considerar. Como máximo parameterLengthMax de largo
					if (parameter.getValue() != null && !StringUtils.isNotEmpty(parameter.getValue().toString())){
						if (parameter.getValue().toString().length() > parametersLenght){
							parameterValue = parameter.getValue().toString().substring(0, parametersLenght);
						}else{
							parameterValue = parameter.getValue().toString();
						}
					}
					logErrorFilter.getParameters().put(parameter.getKey(), parameterValue);
				}
			}
		}
		DTOApplicationErrorData applicationErrorData = logEvent(ap.getApplicationEventLogApi(), logErrorFilter);
		applicationErrorData.setStatusCode(DTOResponseBase.ERROR_STATUS);
		applicationErrorData.setApplicationStatusCodeSource(DTOResponseBase.GLOBALCODE);
		if (exception != null){
			applicationErrorData.setMessage(exception.getMessage());
		}
		else if (customizedEventTitle != null){
			applicationErrorData.setMessage(customizedEventTitle);
		}
		return applicationErrorData;
	}

	public static void logApplicationErrorDataCatched(DTOApplicationErrorData dtoApplicationErrorData){
		logApplicationErrorDataCatched(dtoApplicationErrorData, null, null);
	}


	public static void LogApplicationErrorDataCatched(DTOApplicationErrorData dtoApplicationErrorData, DTOFilter request){
		logApplicationErrorDataCatched(dtoApplicationErrorData, request, null);
	}
	
	public static DTOApplicationErrorData logErrorCatched(Exception exception, DTOFilter inputFilter){
		return logErrorCatched(exception, inputFilter, null, null, null, 5, null, null, 0);
	}
	
	public static DTOApplicationErrorData logErrorCatched(Exception exception){
		return logErrorCatched(exception, null, null, null, null, 5, null, null, 0);
	}

	/**
	 * logApplicationErrorDataCatched
	 * @param dtoApplicationErrorData
	 * @param request
	 * @param eventOrigin
	 */
	public static void logApplicationErrorDataCatched(DTOApplicationErrorData dtoApplicationErrorData, DTOFilter request, String eventOrigin){
		String customizedEventTitle = null;
		if (dtoApplicationErrorData != null){
			customizedEventTitle = dtoApplicationErrorData.getStatusCode() + "-" + dtoApplicationErrorData.getApplicationStatusCodeSource();
		}
		String customizedEventMessage = null;
		if (dtoApplicationErrorData!= null && dtoApplicationErrorData.getStatusCodeParameters() != null){
			customizedEventMessage = ("," + dtoApplicationErrorData.getStatusCodeParameters());
		}
		DTOApplicationErrorData logged = logErrorCatched(null, request, customizedEventTitle, customizedEventMessage, null, 5, eventOrigin, null, 0);
		if (dtoApplicationErrorData != null){
			dtoApplicationErrorData.setApplicationEventTableName(logged.getApplicationEventTableName()); 
			dtoApplicationErrorData.setIdApplicationEventLog(logged.getIdApplicationEventLog());
		}
	}
	
	/**
	 * logErrorCatched
	 * @param exception
	 * @param inputFilter
	 * @param customizedEventTitle
	 * @param customizedEventMessage
	 * @param additionalParams
	 * @param eventSeverity
	 * @param eventOrigin
	 * @param visitorIp
	 * @param paramsLength
	 * @return DTOApplicationErrorData
	 */
	public static DTOApplicationErrorData logErrorCatched(Exception exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, 
			Map<String, Object> additionalParams, int eventSeverity, String eventOrigin, String visitorIp, int paramsLength){
		try{
			return logError(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, eventOrigin, visitorIp, paramsLength);
		}catch (RuntimeException ex){
			DTOApplicationErrorData tempVar = new DTOApplicationErrorData();
			tempVar.setStatusCode(DTOResponseBase.ERROR_STATUS);
			tempVar.setApplicationStatusCodeSource(DTOResponseBase.GLOBALCODE);
			tempVar.setMessage("No respondió la api centralizadora de eventos");
			return tempVar;
		}
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, String customizedEventTitle, Map<String, Object> additionalParams){
		return logErrorCatchedWithNotification(exception, customizedEventTitle, additionalParams, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, String customizedEventTitle, Map<String, Object> additionalParams, boolean notify, int paramsLength){
		return logErrorCatchedWithNotification(exception, null, customizedEventTitle, null, additionalParams, 5, null, null, notify, paramsLength);
	}
	
	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams, int eventSeverity, String eventOrigin, String visitorIp, boolean notify){
		return logErrorCatchedWithNotification(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, eventOrigin, visitorIp, notify, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams, int eventSeverity, String eventOrigin, String visitorIp){
		return logErrorCatchedWithNotification(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, eventOrigin, visitorIp, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams, int eventSeverity, String eventOrigin){
		return logErrorCatchedWithNotification(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, eventOrigin, null, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams, int eventSeverity){
		return logErrorCatchedWithNotification(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, null, null, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, java.util.Map<String, Object> additionalParams){
		return logErrorCatchedWithNotification(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, 5, null, null, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage){
		return logErrorCatchedWithNotification(exception, inputFilter, customizedEventTitle, customizedEventMessage, null, 5, null, null, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle){
		return logErrorCatchedWithNotification(exception, inputFilter, customizedEventTitle, null, null, 5, null, null, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter){
		return logErrorCatchedWithNotification(exception, inputFilter, null, null, null, 5, null, null, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception){
		return logErrorCatchedWithNotification(exception, null, null, null, null, 5, null, null, false, 0);
	}

	public static DTOApplicationErrorData logErrorCatchedWithNotification(){
		return logErrorCatchedWithNotification(null, null, null, null, null, 5, null, null, false, 0);
	}

	/**
	 * logErrorCatchedWithNotification
	 * @param exception
	 * @param inputFilter
	 * @param customizedEventTitle
	 * @param customizedEventMessage
	 * @param additionalParams
	 * @param eventSeverity
	 * @param eventOrigin
	 * @param visitorIp
	 * @param notify
	 * @param paramsLength
	 * @return DTOApplicationErrorData
	 */
	public static DTOApplicationErrorData logErrorCatchedWithNotification(RuntimeException exception, DTOFilter inputFilter, String customizedEventTitle, String customizedEventMessage, Map<String, Object> additionalParams, int eventSeverity, String eventOrigin, String visitorIp, boolean notify, int paramsLength){
		DTOApplicationErrorData dtoErrorData = logErrorCatched(exception, inputFilter, customizedEventTitle, customizedEventMessage, additionalParams, eventSeverity, eventOrigin, visitorIp, paramsLength);
		if (notify){
			if (inputFilter == null){
				inputFilter = new DTOFilter();
			}
			inputFilter.getParameters().put("dtoErrorData", dtoErrorData);
			inputFilter.getParameters().put("EmailListCode", additionalParams != null && additionalParams.containsKey("EmailListCode") ? additionalParams.get("EmailListCode") : "GenericErrorNotify");
			inputFilter.getParameters().put("ModuleCode", additionalParams != null && additionalParams.containsKey("ModuleCode") ? additionalParams.get("ModuleCode") : "GATEWAYS");
			inputFilter.getParameters().put("TemplateCode", additionalParams != null && additionalParams.containsKey("TemplateCode") ? additionalParams.get("TemplateCode") : "GenericErrorTemplate");
			inputFilter.getParameters().put("CountryCode", 540);
			//TODO ApiEmailAdminHelper.SendEmailDynamic(inputFilter);
		}
		return dtoErrorData;
	}

	/**
	 * logEvent
	 * @param applicationEventLogApi
	 * @param filterLog
	 * @return DTOApplicationErrorData
	 */
	private static DTOApplicationErrorData logEvent(String applicationEventLogApi, DTOFilter filterLog){
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		filterLog.getParameters().put("LogCentralV2", true);
		String relativeUrl = "Logger/LogApplicationEvent";
		String jsonContext = gson.toJson(filterLog);
		String response = ApiHelper.post(applicationEventLogApi, relativeUrl, jsonContext);
		return ResponseTypesHelper.responseEntity(response, DTOApplicationErrorData.class);
	}
	
	
	/**
	 * getEnviromentLogConfigurationKeys
	 * @param ap
	 */
	private static void getEnviromentLogConfigurationKeys(ApplicationProps ap){
		if (wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventLogApi) != null)
			ap.setApplicationEventLogApi(wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventLogApi));
		if (wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventsTableName) != null)
			ap.setApplicationEventsTableName(wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventsTableName));
		if (wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationName) != null)
			ap.setApplicationName(wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationName));
	}
}
	
class ApplicationProps{
	String applicationEventLogApi;
	String ApplicationEventsTableName;
	String ApplicationName;
	
	public String getApplicationEventLogApi() {
		return applicationEventLogApi;
	}
	public void setApplicationEventLogApi(String applicationEventLogApi) {
		this.applicationEventLogApi = applicationEventLogApi;
	}
	public String getApplicationEventsTableName() {
		return ApplicationEventsTableName;
	}
	public void setApplicationEventsTableName(String applicationEventsTableName) {
		ApplicationEventsTableName = applicationEventsTableName;
	}
	public String getApplicationName() {
		return ApplicationName;
	}
	public void setApplicationName(String applicationName) {
		ApplicationName = applicationName;
	}
}