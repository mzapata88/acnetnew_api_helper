package com.assistcardHelper.dtoMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.assistcardHelper.configuration.WSInvokeConfigurationConst;
import com.assistcardHelper.service.impl.WSInvokeConfigurationServiceImpl;

@Component
public class ApplicationEventLogConfigurationHelper {
	
	private static WSInvokeConfigurationServiceImpl wSInvokeConfServiceImpl;

	@Autowired
	public ApplicationEventLogConfigurationHelper(WSInvokeConfigurationServiceImpl wSInvoke) {
		ApplicationEventLogConfigurationHelper.wSInvokeConfServiceImpl = wSInvoke;
	}
	
	public static String getApplicationEventLogApi(){
		return (wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventLogApi) !=null)? wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventLogApi): null;
	}
	
	public static String getApplicationName(){
		return (wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationName) !=null)? wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationName): null;
	}

	public static String getApplicationEventsTablesPrefix(){
		return (wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventsTablesPrefix) !=null)? wSInvokeConfServiceImpl.getInstance().get(WSInvokeConfigurationConst.applicationEventsTablesPrefix): null;
	}
}
