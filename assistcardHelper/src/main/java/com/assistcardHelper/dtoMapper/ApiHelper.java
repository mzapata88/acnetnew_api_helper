package com.assistcardHelper.dtoMapper;

import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class ApiHelper {

	public static String post(String baseUrl, String relativeUrl, String jsonData, int timeout){
		return post(baseUrl, relativeUrl, jsonData, timeout, null);
	}

	public static String post(String baseUrl, String relativeUrl, String jsonData){
		return post(baseUrl, relativeUrl, jsonData, 0, null);
	}

	public static <T> String post(String baseUrl, String relativeUrl, String jsonData, int timeout, HashMap<String,String> headers){
		try{
			String url = (relativeUrl!=null && !relativeUrl.isEmpty())? joinedUrl(baseUrl, relativeUrl): baseUrl;
			RestTemplate restTemplate = new RestTemplate();
			URI uri = new URL(url).toURI();
			BodyBuilder contentType = RequestEntity.post(uri).contentType(MediaType.APPLICATION_JSON);
			if(headers!=null) {
				HttpHeaders httpHeaders = new HttpHeaders();
				for (Map.Entry<String, String> set : headers.entrySet()) {
					httpHeaders.add(set.getKey(), set.getValue());
				}
				contentType.headers(httpHeaders);
			}
			RequestEntity<String> requestEntity = contentType.body(jsonData); 
			ResponseEntity<String> response = restTemplate.exchange(requestEntity, String.class);
			return response.getBody();
		}catch (Exception e) {
			return null;
		}
	}

	private static String joinedUrl(String baseUrl, String relativeUrl){
		String urlUnion = "";
		if (!baseUrl.endsWith("/") && !relativeUrl.startsWith("/")){
			urlUnion = "/";
		}
		return baseUrl + urlUnion + relativeUrl;
	}
}