package com.assistcardHelper.dtoMapper;

public class DTOConnectionStringData extends DTOBase{
	
	private String name;
	private String dataSource;
	private String initialCatalog;
	private String userId;
	private boolean connectionIsOK;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDataSource() {
		return dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	public String getInitialCatalog() {
		return initialCatalog;
	}
	public void setInitialCatalog(String initialCatalog) {
		this.initialCatalog = initialCatalog;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public boolean isConnectionIsOK() {
		return connectionIsOK;
	}
	public void setConnectionIsOK(boolean connectionIsOK) {
		this.connectionIsOK = connectionIsOK;
	}
}