package com.assistcardHelper.dtoMapper;

import org.apache.commons.lang.StringUtils;
import com.google.gson.reflect.TypeToken;
import com.assistcardHelper.util.exception.CustomizedException;
import com.assistcardHelper.util.generic.response.dto.DTOApplicationErrorData;
import com.assistcardHelper.util.generic.response.dto.DTOResponseBase;
import com.assistcardHelper.util.generic.response.dto.DTOResponseEntity;
import com.assistcardHelper.util.generic.response.dto.DTOResponseExecution;
import com.assistcardHelper.util.generic.response.dto.DTOResponseInsert;
import com.assistcardHelper.util.generic.response.dto.DTOResponseList;
import com.assistcardHelper.util.generic.response.dto.DTOResponseListScalar;
import com.assistcardHelper.util.generic.response.dto.DTOResponseScalar;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.LinkedTreeMap;

import java.lang.reflect.Type;
import java.util.List;

public class ResponseTypesHelper {
	public static String NULLDATAEXCEPTION = "NULLDATA";
	public static String STATUSCODE = "STS";

	public static <T> List<T>responseList(String response){
		return ResponseTypesHelper.<T>responseList(response, null);
	}

	/**
	 * responseList
	 * @param <T>
	 * @param response
	 * @param type
	 * @return <T>
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> responseList(String response, Class<T> type){
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		DTOResponseList<T> dtoResponseList = gson.fromJson(response, DTOResponseList.class);
		if (type !=null) {
			Type typeToken = new TypeToken<DTOResponseList<T>>(){}.getType();
			dtoResponseList = gson.fromJson(response.toString(), typeToken);
		}
		if (dtoResponseList.getStatusCode() == DTOResponseBase.EXECUTED_OK_STATUS){
			return dtoResponseList.getResponseList();
		}
		throw throwException(dtoResponseList);
	}

	public static <T> T responseEntity(String response){
		return ResponseTypesHelper.<T>responseEntity(response, null);
	}

	/**
	 * @param <T>
	 * @param response
	 * @param type
	 * @return 
	 * @return <T>
	 */
	@SuppressWarnings("unchecked")
	public static <T> T responseEntity(String response, Class<T> type){
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		DTOResponseEntity<T> dtoResponseEntity = gson.fromJson(response, DTOResponseEntity.class);
		if (type !=null) {
			Type typeToken = new TypeToken<DTOResponseEntity<T>>(){}.getType();
			dtoResponseEntity = gson.fromJson(response.toString(), typeToken);
		}
		if (dtoResponseEntity.getStatusCode() == DTOResponseBase.EXECUTED_OK_STATUS){
			if(dtoResponseEntity.getResponseEntity() instanceof LinkedTreeMap && type !=null) {
				return gson.fromJson(dtoResponseEntity.getResponseEntity().toString(), type);
			}else {
				return dtoResponseEntity.getResponseEntity();				
			}
		}
		throw throwException(dtoResponseEntity);
	}

	/**
	 * responseListScalar
	 * @param <T>
	 * @param response
	 * @return List<T>
	 */
	@SuppressWarnings("unchecked")
	public static <T> List<T> responseListScalar(String response){ //T seria INT o STRING
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		DTOResponseListScalar<T> dtoResponseListScalar = gson.fromJson(response, DTOResponseListScalar.class);
		if (dtoResponseListScalar.getStatusCode() == DTOResponseBase.EXECUTED_OK_STATUS){
			return dtoResponseListScalar.getResponseListScalar();
		}
		throw throwException(dtoResponseListScalar);
	}

	@SuppressWarnings("unchecked")
	public static <T> T responseScalar(String response){ //T seria INT o STRING
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		DTOResponseScalar<T> dtoResponseScalar = gson.fromJson(response, DTOResponseScalar.class);
		if (dtoResponseScalar.getStatusCode() == DTOResponseBase.EXECUTED_OK_STATUS){
			return dtoResponseScalar.getResponseScalar();
		}
		throw throwException(dtoResponseScalar);
	}

	public static void responseExecution(String response){
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		DTOResponseExecution dtoResponseExecution = gson.fromJson(response, DTOResponseExecution.class);
		if (dtoResponseExecution.getStatusCode() == DTOResponseBase.EXECUTED_OK_STATUS){
			return;
		}
		throw throwException(dtoResponseExecution);
	}

	public static int responseInsert(String response){
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).setPrettyPrinting().create();
		DTOResponseInsert dtoResponseInsert = gson.fromJson(response, DTOResponseInsert.class);
		if (dtoResponseInsert.getStatusCode() == DTOResponseInsert.INSERTED_OK){
			return dtoResponseInsert.getRegisteredId();
		}
		throw throwException(dtoResponseInsert);
	}

	public static CustomizedException throwException(DTOResponseBase dtoResponseBase){ //debe ser público para ser llamado desde afuera los dto de respuesta customizados
		return new CustomizedException(dtoResponseBase.getStatusCode(), dtoResponseBase.getApplicationStatusCodeSource(),
				dtoResponseBase.getApplicationEventTableName(), dtoResponseBase.getErrorId(), dtoResponseBase.getStatusCodeParameters(), dtoResponseBase.getMessage());
	}

	//A las UI se mostrará el id del error, y antes un código que representa la tabla en la que fue logueado
	//Ese código se compone de las mayúsculas de dicha tabla. 
	//Por ejemplo, el log con id=100 de la tabla TelemedMedicalSpecialistApiCall, será : APAC100
	public static String getApplicationEventLogUICode(DTOApplicationErrorData applicationErrorData){
		if (applicationErrorData != null){
			if (StringUtils.isNotEmpty(applicationErrorData.getApplicationEventTableName())){
				String accumulatedApplicationEventLogUICode = "";
				String inputString = applicationErrorData.getApplicationEventTableName();
				for (int i = 0; i < inputString.length(); i++) {
					char c = inputString.charAt(i);
					if (Character.isUpperCase(c)){
						accumulatedApplicationEventLogUICode += c;
					} 
				}
				return accumulatedApplicationEventLogUICode + applicationErrorData.getIdApplicationEventLog();
			}
			if (StringUtils.isNotEmpty(applicationErrorData.getApplicationStatusCodeSource()) && applicationErrorData.getStatusCode() > 0){
				String appName = applicationErrorData.getApplicationStatusCodeSource().replace(".", ""); //Quito los puntos, porque este valor queda en la url, y las rutas no responden si la url tiene un punto
				return String.format("%1$s-%2$s-%3$s", STATUSCODE, appName, applicationErrorData.getStatusCode());
			}
		}
		return NULLDATAEXCEPTION;
	}
}
