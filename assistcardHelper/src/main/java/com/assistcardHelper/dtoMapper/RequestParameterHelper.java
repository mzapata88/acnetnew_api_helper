package com.assistcardHelper.dtoMapper;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

import com.assistcardHelper.util.enums.GeneralStatusCodesEnum;
import com.assistcardHelper.util.exception.CustomizedException;
import com.assistcardHelper.util.generic.response.dto.DTOResponseBase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;


public class RequestParameterHelper{

	/**
	 * ParameterFlags
	 */
	public enum ParameterFlags {
		NoRestriction(1),
		ForbiddenParameter(2),
		ForbiddenParameterValue(4),
		ForcedParameterValue(8),
		NumericCantBePositive(16),
		NumericCantBeZero(32),
		NumericCantNegative(64);
		private int intValue;
		private ParameterFlags(int value){
			intValue = value;
		}
		public int getValue(){
			return intValue;
		}
	}

	/**
	 * ParameterCombinationFlags
	 */
	public enum ParameterCombinationFlags{
		AtLeastOneMustBeAssigned(1), //Al menos uno de la lista debe ser asignado
		OneAsTooManyCanBeAssigned(2), //Como mucho uno puede ser asignado
		AllOrNoneMustBeAssigned(4); //Todos deben ser asignados o ninguno
		private int intValue;
		private ParameterCombinationFlags(int value){
			intValue = value;
		}
		public int getValue(){
			return intValue;
		}
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags, T flagRestrictionParameter, boolean isCreatingTestingCollection, Class<T> type){
		return getGenericParameter(filter, parameterName, parameterFlags, flagRestrictionParameter, isCreatingTestingCollection, null,type);
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags, T flagRestrictionParameter, Class<T> type){
		return getGenericParameter(filter, parameterName, parameterFlags, flagRestrictionParameter, false, null,type);
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags, Class<T> type){
		return getGenericParameter(filter, parameterName, parameterFlags, null, false, null,type);
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, Class<T> type){
		return getGenericParameter(filter, parameterName, ParameterFlags.NoRestriction, null, false, null, type);
	}
	
	/**
	 * getGenericParameter
	 * @param <T>
	 * @param filter
	 * @param parameterName
	 * @param parameterFlags
	 * @param flagRestrictionParameter
	 * @param isCreatingTestingCollection
	 * @param suggestedValueForTestingCollection
	 * @param type
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getGenericParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags, T flagRestrictionParameter, boolean isCreatingTestingCollection, T suggestedValueForTestingCollection, Class<T> type)
	{
		checkFilterWithParametersHasValue(filter);
		checkParameterHasValue(filter, parameterName);
		Object objectValue = filter.getParameters().get(parameterName);	
		T value;
	    // Generamos el objecto Gson para manipular los json.
		GsonBuilder builder = new GsonBuilder();
	    Gson gson = builder.enableComplexMapKeySerialization().create();
		try {
			if( jsonCanConvertType(type) ) {
				value = (T) (type.cast(objectValue));
			}
			else{
				if (objectValue == type){
					value = (T)objectValue;
				}
				else if ( type instanceof Class /*|| typeof(T).IsGenericType*/) {
					 JsonElement data = gson.fromJson(objectValue.toString(), JsonElement.class);
					 String jsonInString = gson.toJson(data);
				      System.out.println(jsonInString);
			          if( type == String.class) {
                         value = (T) jsonInString;
			          }else if(Collection.class.isAssignableFrom(type)) {
			        	Type ttype = new TypeToken<ArrayList<T>>(){}.getType();
			  			value = gson.fromJson(data, ttype);  
                      }else {
                         value = (T)gson.fromJson(data, type);
                      }
				}
				else{
					throw new RuntimeException("No hay método de deserialización seteado");
				}
			}
		}catch (RuntimeException e){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.ParameterUnableToDeserialize.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
		}		
		return value;
	}
	
	/**
	 * CheckFilterWithParametersHasValue
	 * @param filter
	 */
	private static void checkFilterWithParametersHasValue(DTOFilter filter){
		if (filter == null || filter.getParameters() == null){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.FilterIsNull.toString()), DTOResponseBase.GLOBALCODE);
		}
	 }
		
	/**
	 * checkParameterForbiddenValue
	 * @param <T>
	 * @param parameterName
	 * @param objectValue
	 * @param forbiddenValue
	 */
	@SuppressWarnings("unused")
	private static <T> void checkParameterForbiddenValue(String parameterName, T objectValue, T forbiddenValue)
	{
		if (forbiddenValue == null){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.FlagParameterMissingForParameterRestriction.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
		}
		if (jsonCanConvertType(objectValue.getClass())){
			if (objectValue.toString().equals(forbiddenValue.toString())){
				throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.ParameterForbiddenValue.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
			}
		}else{
			throw new RuntimeException("No se puede prohibir un valor de objetos customizados");
		}
	}

	/**
	 * checkParameterForcedValue
	 * @param <T>
	 * @param parameterName
	 * @param objectValue
	 * @param forcedValue
	 */
	@SuppressWarnings("unused")
	private static <T> void checkParameterForcedValue(String parameterName, T objectValue, T forcedValue){
		if (forcedValue == null){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.FlagParameterMissingForParameterRestriction.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
		}
		if (jsonCanConvertType(objectValue.getClass())){
			if (!objectValue.toString().equals(forcedValue.toString())){
				throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.ParameterForcedValueNotMatch.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
			}
		}else{
			throw new RuntimeException("No se puede forzar un valor de objetos customizados");
		}
	}
		
	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags, T flagRestrictionParameter, boolean isCreatingTestingCollection)
	{
		return getGenericOptionalParameter(filter, parameterName, parameterFlags, flagRestrictionParameter, isCreatingTestingCollection, null);
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags, T flagRestrictionParameter)
	{
		return getGenericOptionalParameter(filter, parameterName, parameterFlags, flagRestrictionParameter, false, null);
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags)
	{
		return getGenericOptionalParameter(filter, parameterName, parameterFlags, null, false, null);
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName)
	{
		return getGenericOptionalParameter(filter, parameterName, ParameterFlags.NoRestriction, null, false, null);
	}

	public static <T> T getGenericOptionalParameter(DTOFilter filter, String parameterName, ParameterFlags parameterFlags, 
			T flagRestrictionParameter, boolean isCreatingTestingCollection, T suggestedValueForTestingCollection, Class<T> type){
		checkFilterWithParametersHasValue(filter);
		if (!filter.getParameters().containsKey(parameterName) || filter.getParameters().get(parameterName) == null){
			return null;
		}
		return getGenericParameter(filter, parameterName, parameterFlags, flagRestrictionParameter, isCreatingTestingCollection, suggestedValueForTestingCollection, type);
	}

	/**
	 * checkParameterIsNotNegative
	 * @param <T>
	 * @param value
	 * @param parameterName
	 */
	@SuppressWarnings("unused")
	private static <T> void checkParameterIsNotNegative(T value, String parameterName){
		boolean isValid = false;
		if (getUnderlyingType(value.getClass()) == Integer.class){
			isValid = Integer.parseInt(value.toString()) >= 0;
		}
		if (getUnderlyingType(value.getClass()) == BigDecimal.class){
			isValid = new BigDecimal(value.toString()).compareTo(BigDecimal.ZERO) >= 0;
		}
		if (getUnderlyingType(value.getClass()) == Long.class){
			isValid = Long.parseLong(value.toString()) >= 0;
		}
		if (getUnderlyingType(value.getClass()) == Short.class){
			isValid = Short.parseShort(value.toString()) >= 0;
		}
		if (getUnderlyingType(value.getClass()) == Float.class){
			isValid = Float.parseFloat(value.toString()) >= 0;
		}
		if (!isValid){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.ParameterMustBePositive.toString()), 
					DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
		}
	}

	/**
	 * CheckParameterIsNotZero
	 * @param <T>
	 * @param value
	 * @param parameterName
	 */
	@SuppressWarnings("unused")
	private static <T> void checkParameterIsNotZero(T value, String parameterName){
		Boolean isValid = false;
		if (getUnderlyingType(value.getClass()) == Integer.class){
			isValid = Integer.parseInt(value.toString()) != 0;
		}
		if (getUnderlyingType(value.getClass()) == BigDecimal.class){
			isValid = new BigDecimal(value.toString()).compareTo(BigDecimal.ZERO) != 0;
		}
		if (getUnderlyingType(value.getClass()) == Long.class){
			isValid = Long.parseLong(value.toString()) != 0;
		}
		if (getUnderlyingType(value.getClass()) == Short.class){
			isValid = Short.parseShort(value.toString()) != 0;
		}
		if (getUnderlyingType(value.getClass()) == Float.class){
			isValid = Float.parseFloat(value.toString()) != 0;
		}
		if (!isValid){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.ParameterMustBePositive.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
		}
	}

	/**
	 * jsonCanConvertType
	 * @param convertType
	 * @return Boolean
	 */
	private static Boolean jsonCanConvertType(Class <?> convertType){
		return convertType == String.class || convertType == Integer.class || convertType == Boolean.class 
			   || convertType == Double.class || convertType == Float.class;
	}

	private static Class<?> getUnderlyingType(Class <?> originalType){
		return originalType;
	}

	/**
	 * checkParameterHasValue
	 * @param filter
	 * @param parameterName
	 */
	private static void checkParameterHasValue(DTOFilter filter, String parameterName){
		if (!filter.getParameters().containsKey(parameterName) || filter.getParameters().get(parameterName) == null){
			throw new CustomizedException(GeneralStatusCodesEnum.ParameterMissing.getCode(),DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
		}
	}

	/**
	 * checkForbiddenParameter
	 * @param filter
	 * @param parameterName
	 */
	public static void checkForbiddenParameter(DTOFilter filter, String parameterName){
		if (filter.getParameters().containsKey(parameterName)){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.ParameterForbidden.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(parameterName)));
		}
	}

	/**
	 * checkParameterCombination
	 * @param parameterCombinationFlags
	 * @param parametersAssignedValues
	 */
	public static void checkParameterCombination(ParameterCombinationFlags parameterCombinationFlags, 
			Map<String, Object> parametersAssignedValues){
		int qtyNotAssigned = 0;
		int qtyAssigned = 0;
		ArrayList<String> parameterListName = new ArrayList<String>();
		
		for (Map.Entry<String, Object> parameterAssignedValues : parametersAssignedValues.entrySet()){
			parameterListName.add(parameterAssignedValues.getKey());
			if (parameterAssignedValues.getValue() == null){
				qtyNotAssigned++;
			}else{
				qtyAssigned++;
			}
		}

		if (parameterCombinationFlags.toString().equals(ParameterCombinationFlags.AtLeastOneMustBeAssigned.toString())){
			if (qtyAssigned == 0){
				throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.FilterCombinationAtLeastOneMissing.toString()), DTOResponseBase.GLOBALCODE, parameterListName);
			}
		}

		if (parameterCombinationFlags.toString().equals(ParameterCombinationFlags.OneAsTooManyCanBeAssigned.toString()))
		{
			if (qtyAssigned > 1)
			{
				throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.FilterCombinationOneAsTooManyExceded.toString()), DTOResponseBase.GLOBALCODE, parameterListName);
			}
		}
		if (parameterCombinationFlags.toString().equals(ParameterCombinationFlags.AllOrNoneMustBeAssigned.toString())){
			if (qtyAssigned != 0 && qtyNotAssigned != 0) //deben ser todos asignados, o ninguno asignado
			{
				throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.FilterCombinationAllOrNoneMustBeAssigned.toString()), DTOResponseBase.GLOBALCODE, parameterListName);
			}
		}
	}

	/**
	 * checkFilterAndParameterHasValue
	 * @param filter
	 * @param parameterName
	 */
	public static void checkFilterAndParameterHasValue(DTOFilter filter, String parameterName){
		checkFilterWithParametersHasValue(filter);
		checkParameterHasValue(filter, parameterName);
	}

	/**
	 * checkParameter
	 * @param <T>
	 * @param parameter
	 * @param required
	 * @param fieldName
	 * @return String
	 */
	public static <T> String checkParameter(String parameter, boolean required, String fieldName){
		if (parameter == null && required){
			throw new CustomizedException(Integer.valueOf(GeneralStatusCodesEnum.ParameterRequiredField.toString()), DTOResponseBase.GLOBALCODE, new ArrayList<String>(Arrays.asList(fieldName)));
		}
		return parameter;
	}
		
}
