package com.assistcardHelper.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assistcardHelper.model.WSInvokeConfiguration;

@Repository
public interface WSInvokeConfigurationDAO extends JpaRepository<WSInvokeConfiguration, Integer>{

}
