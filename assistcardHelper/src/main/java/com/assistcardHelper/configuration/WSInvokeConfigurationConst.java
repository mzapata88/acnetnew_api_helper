package com.assistcardHelper.configuration;

public class WSInvokeConfigurationConst {
	public final static String applicationEventLogApi = "applicationEvent.LogApi";
	public final static String applicationEventsTableName = "applicationEvents.TableName";
	public final static String applicationName = "application.Name";
	public static final String applicationEventsTablesPrefix = "applicationEvents.TablesPrefix";
}
